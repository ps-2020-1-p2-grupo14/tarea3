struct colaTDA
{
    unsigned long tamano;
    struct nodo_colaTDA *inicio;
    struct nodo_colaTDA *fin;
};
typedef struct colaTDA cola;

struct nodo_colaTDA
{
    void *elemento;
    struct nodo_colaTDA *siguiente;
    struct nodo_colaTDA *anterior;
};
typedef struct nodo_colaTDA nodo_colaTDA;

void crear_cola(cola *cola);

int encolar(cola *mi_cola, void *elemento);

void *decolar(cola *mi_cola);

unsigned long tamano_cola(cola *mi_cola);

unsigned long posicion_cola(cola *mi_cola, void *elemento);

int destruir_cola(cola *mi_cola);

int isEmpty(cola *mi_cola);
