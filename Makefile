CC = gcc -Wall -c

bin/programa: obj/Main.o obj/LinkedList.o
	gcc $^ -o $@

obj/Main.o: src/Main.c
	$(CC) $^ -I include/ -o $@

obj/LinkedList.o: src/LinkedList.c
	$(CC) $^ -o $@

.PHONY: clean
clean:
	rm bin/programa obj/*.o
